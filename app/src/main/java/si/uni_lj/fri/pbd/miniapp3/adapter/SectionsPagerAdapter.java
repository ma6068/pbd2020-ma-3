package si.uni_lj.fri.pbd.miniapp3.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import si.uni_lj.fri.pbd.miniapp3.ui.search.SearchFragment;
import si.uni_lj.fri.pbd.miniapp3.ui.favorites.FavoritesFragment;


public class SectionsPagerAdapter extends FragmentStateAdapter {

    // number of tabs
    private int tabCount;

    public SectionsPagerAdapter(@NonNull FragmentActivity fragmentActivity, int numOfTabs) {
        super(fragmentActivity);
        tabCount = numOfTabs;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        // if the first tab is clicked => return SearchFragment
        if (position == 0) {
            return new SearchFragment();
        }
        // if the second tab is clicked => return FavoritesFragment
        return new FavoritesFragment();
    }

    @Override
    public int getItemCount() {
        return tabCount;
    }
}
