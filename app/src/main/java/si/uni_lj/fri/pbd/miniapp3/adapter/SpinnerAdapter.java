package si.uni_lj.fri.pbd.miniapp3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO;

public class SpinnerAdapter extends BaseAdapter {

    private Context context;
    // list that have all ingredients names in a String List
    private List<IngredientDTO> ingredients;

    public SpinnerAdapter(Context context, List<IngredientDTO> ingredients) {
        super();
        this.ingredients = ingredients;
        this.context = context;
    }

    @Override
    public int getCount() {
        return ingredients.size();
    }

    @Override
    public IngredientDTO getItem(int position) {
        return ingredients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.spinner_item, parent, false);
        }
        // set a textView with a name of the ingredient
        TextView textView = convertView.findViewById(R.id.text_view_spinner_item);
        String ingredientName = ingredients.get(position).getStrIngredient();
        if (ingredientName != null) {
            textView.setText(ingredientName);
        }

        return convertView;
    }
}
