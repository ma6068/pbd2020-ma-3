package si.uni_lj.fri.pbd.miniapp3.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.ui.DetailsActivity;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.CardViewHolder> {

    private List<RecipeSummaryIM> recipes;
    private Context context;
    // shows whether the RecyclerViewAdapter was instantiated from the SearchFragment or from the FavouritesFragment
    private String whichFragment;

    public RecyclerViewAdapter(Context context, List<RecipeSummaryIM> recipes, String whichFragment) {
        this.context = context;
        this.recipes = recipes;
        this.whichFragment = whichFragment;
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;

        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_view);
            textView = itemView.findViewById(R.id.text_view_content);
        }
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_grid_item, viewGroup, false);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull CardViewHolder viewHolder, int position) {
        // set the image from url via Glide
        Glide.with(context).load(recipes.get(position).getStrMealThumb()).into(viewHolder.imageView);
        // set the textView
        viewHolder.textView.setText(recipes.get(position).getStrMeal());
        viewHolder.imageView.setOnClickListener(v -> {
            // share recipe data via intent
            Intent intent = new Intent(context, DetailsActivity.class);
            intent.putExtra("idMeal", recipes.get(position).getIdMeal());
            intent.putExtra("whichFragment", whichFragment);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

}
