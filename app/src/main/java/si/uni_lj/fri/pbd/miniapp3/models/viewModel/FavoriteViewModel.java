package si.uni_lj.fri.pbd.miniapp3.models.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.database.Database;
import si.uni_lj.fri.pbd.miniapp3.database.dao.RecipeDao;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;

public class FavoriteViewModel extends AndroidViewModel {

    private RecipeDao recipeDao;

    public FavoriteViewModel(@NonNull Application application) {
        super(application);
        Database database = Database.getDatabase(application);
        this.recipeDao = database.recipeDao();
    }

    public RecipeDetails getRecipeDetailsById (String idMeal) {
        return recipeDao.getRecipeById(idMeal);
    }

    public List<RecipeDetails> getAllFavoriteRecipes() {
        return recipeDao.getAllFavorites();
    }

    public void insert(RecipeDetails recipeDetails) {
        recipeDao.insertRecipe(recipeDetails);
    }

    public void delete(String idMeal) {
        recipeDao.delete(idMeal);
    }
}
