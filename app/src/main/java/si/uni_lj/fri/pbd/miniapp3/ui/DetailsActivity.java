package si.uni_lj.fri.pbd.miniapp3.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.viewModel.FavoriteViewModel;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeDetailsIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeDetailsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIdDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;

public class DetailsActivity extends AppCompatActivity {

    private String id;
    private String whichFragment;

    private Button button;
    private TextView mealNameView;
    private ImageView imageView;
    private TextView ingredientsView;
    private TextView measurementsView;
    private TextView instructions;
    private TextView errorView;

    private List<RecipeDetailsDTO> recipe;
    private RecipeDetails recipeDetails;
    private FavoriteViewModel favoriteViewModel;
    private RecipeDetailsIM recipeDetailsIM;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // set the views
        button = findViewById(R.id.button);
        mealNameView = findViewById(R.id.mealName);
        imageView = findViewById(R.id.picture);
        ingredientsView = findViewById(R.id.ingredients);
        measurementsView = findViewById(R.id.measurements);
        instructions = findViewById(R.id.instructions);
        errorView = findViewById(R.id.error_details_view);

        // get the data from intent
        Intent intent = getIntent();
        id = intent.getStringExtra("idMeal");
        whichFragment = intent.getStringExtra("whichFragment");

        // set the viewModel
        favoriteViewModel = new ViewModelProvider(this).get(FavoriteViewModel.class);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // the detailsActivity is open via SearchFragment
        if (whichFragment.equals("1")) {
            // get the recipe details from the remote server
            RestAPI restAPI =  ServiceGenerator.createService(RestAPI.class);
            Call<RecipesByIdDTO> call = restAPI.getRecipesById(id);
            call.enqueue(new Callback<RecipesByIdDTO>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(Call<RecipesByIdDTO> call, Response<RecipesByIdDTO> response) {
                    // response is not successful
                    if (!response.isSuccessful()) {
                        Snackbar.make(errorView, "Code: " + response.code(), Snackbar.LENGTH_LONG).show();
                    }
                    // the response is successful
                    else {
                        // get the response (RecipesByIdDTO object)
                        RecipesByIdDTO recipesByIdDTO = response.body();
                        recipe = recipesByIdDTO.getRecipes();
                        if (recipe != null) {
                            recipeDetails = favoriteViewModel.getRecipeDetailsById(recipe.get(0).getIdMeal());
                            // the recipe is not in database => set that is not favorite
                            if (recipeDetails == null) {
                                recipeDetails = Mapper.mapRecipeDetailsDtoToRecipeDetails(false, recipe.get(0));
                                recipeDetailsIM = Mapper.mapRecipeDetailsToRecipeDetailsIm(false, recipeDetails);
                                button.setText("ADD TO FAVORITES");
                            }
                            // the recipe is in database => it's favorite
                            else {
                                recipeDetails = Mapper.mapRecipeDetailsDtoToRecipeDetails(true, recipe.get(0));
                                recipeDetailsIM = Mapper.mapRecipeDetailsToRecipeDetailsIm(true, recipeDetails);
                                button.setText("REMOVE FROM FAVORITES");
                            }
                        }
                        else {
                            Snackbar.make(errorView, "No details for that recipe", Snackbar.LENGTH_LONG).show();
                        }
                    }
                    if (recipeDetailsIM != null) {
                        button.setOnClickListener(v -> {
                            // if is not favorite and click => add in database and set it to favorite
                            if (!recipeDetails.getFavorite()) {
                                // object change
                                recipeDetails.setFavorite(true);
                                // database change
                                favoriteViewModel.insert(recipeDetails);
                                // change button text
                                button.setText("REMOVE FROM FAVORITES");
                            }
                            // if is favorite and click => delete it from database and remove it from favorite
                            else {
                                // object change
                                recipeDetails.setFavorite(false);
                                // database change
                                favoriteViewModel.delete(recipeDetails.getIdMeal());
                                // change button text
                                button.setText("ADD TO FAVORITES");
                            }
                        });
                        // set the views
                        mealNameView.setText(recipeDetailsIM.getStrMeal());
                        Glide.with(getApplicationContext()).load(recipeDetailsIM.getStrMealThumb()).into(imageView);
                        String allIngredients = getAllIngredients(recipeDetailsIM);
                        ingredientsView.setText(allIngredients);
                        String allMeasurements = getAllMeasurements(recipeDetailsIM);
                        measurementsView.setText(allMeasurements);
                        instructions.setText(recipeDetailsIM.getStrInstructions());
                    }
                    // didn't get the recipe details => show some error
                    else {
                        Snackbar.make(errorView, "No details for that recipe", Snackbar.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<RecipesByIdDTO> call, Throwable t) {
                    Snackbar.make(errorView, "No internet connection", Snackbar.LENGTH_LONG).show();
                }
            });
        }
        else if (whichFragment.equals("2")){
            // get recipe details from the local database and the ViewModel (recipe must be favorite)
            recipeDetails = favoriteViewModel.getRecipeDetailsById(id);
            if (recipeDetails != null) {
                button.setText("REMOVE FROM FAVORITES");
                recipeDetailsIM = Mapper.mapRecipeDetailsToRecipeDetailsIm(true, recipeDetails);
            }
            else {
                Snackbar.make(errorView, "No details for that recipe", Snackbar.LENGTH_LONG).show();
            }
            // recipe is / is not favorite (if click more times)
            if (recipeDetailsIM != null) {
                button.setOnClickListener(v -> {
                    // if is not favorite and click => add it in database and set that is favorite
                    if (!recipeDetails.getFavorite()) {
                        // object change
                        recipeDetails.setFavorite(true);
                        // database change
                        favoriteViewModel.insert(recipeDetails);
                        // change button text
                        button.setText("REMOVE FROM FAVORITES");
                    }
                    // if is favorite and click => delete it from database and set that is not favorite
                    else {
                        // object change
                        recipeDetails.setFavorite(false);
                        // database change
                        favoriteViewModel.delete(recipeDetails.getIdMeal());
                        // change button text
                        button.setText("ADD TO FAVORITES");
                    }

                });
                // set the views
                mealNameView.setText(recipeDetailsIM.getStrMeal());
                Glide.with(getApplicationContext()).load(recipeDetailsIM.getStrMealThumb()).into(imageView);
                String allIngredients = getAllIngredients(recipeDetailsIM);
                ingredientsView.setText(allIngredients);
                String allMeasurements = getAllMeasurements(recipeDetailsIM);
                measurementsView.setText(allMeasurements);
                instructions.setText(recipeDetailsIM.getStrInstructions());
            }
        }
    }


    // function that gives back a String with all ingredients
    public String getAllIngredients(RecipeDetailsIM recipeDetailsIM) {
        String allIngredients = "";
        boolean firstIngredient = true;

        if (recipeDetailsIM.getStrIngredient1() != null && !(recipeDetailsIM.getStrIngredient1().trim().length() == 0)) {
            firstIngredient = false;
            allIngredients = recipeDetailsIM.getStrIngredient1();
        }
        if (recipeDetailsIM.getStrIngredient2() != null && !(recipeDetailsIM.getStrIngredient2().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient2();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient2();
            }
        }
        if (recipeDetailsIM.getStrIngredient3() != null && !(recipeDetailsIM.getStrIngredient3().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient3();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient3();
            }
        }
        if (recipeDetailsIM.getStrIngredient4() != null && !(recipeDetailsIM.getStrIngredient4().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient4();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient4();
            }
        }
        if (recipeDetailsIM.getStrIngredient5() != null && !(recipeDetailsIM.getStrIngredient5().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient5();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient5();
            }
        }
        if (recipeDetailsIM.getStrIngredient6() != null && !(recipeDetailsIM.getStrIngredient6().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient6();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient6();
            }
        }
        if (recipeDetailsIM.getStrIngredient7() != null && !(recipeDetailsIM.getStrIngredient7().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient7();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient7();
            }
        }
        if (recipeDetailsIM.getStrIngredient8() != null && !(recipeDetailsIM.getStrIngredient8().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient8();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient8();
            }
        }
        if (recipeDetailsIM.getStrIngredient9() != null && !(recipeDetailsIM.getStrIngredient9().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient9();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient9();
            }
        }
        if (recipeDetailsIM.getStrIngredient10() != null && !(recipeDetailsIM.getStrIngredient10().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient10();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient10();
            }
        }
        if (recipeDetailsIM.getStrIngredient11() != null && !(recipeDetailsIM.getStrIngredient11().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient11();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient11();
            }
        }
        if (recipeDetailsIM.getStrIngredient12() != null && !(recipeDetailsIM.getStrIngredient12().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient12();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient12();
            }
        }
        if (recipeDetailsIM.getStrIngredient13() != null && !(recipeDetailsIM.getStrIngredient13().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient13();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient13();
            }
        }
        if (recipeDetailsIM.getStrIngredient14() != null && !(recipeDetailsIM.getStrIngredient14().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient14();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient14();
            }
        }
        if (recipeDetailsIM.getStrIngredient15() != null && !(recipeDetailsIM.getStrIngredient15().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient15();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient15();
            }
        }
        if (recipeDetailsIM.getStrIngredient16() != null && !(recipeDetailsIM.getStrIngredient16().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient16();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient16();
            }
        }
        if (recipeDetailsIM.getStrIngredient17() != null && !(recipeDetailsIM.getStrIngredient17().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient17();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient17();
            }
        }
        if (recipeDetailsIM.getStrIngredient18() != null && !(recipeDetailsIM.getStrIngredient18().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient18();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient18();
            }
        }
        if (recipeDetailsIM.getStrIngredient19() != null && !(recipeDetailsIM.getStrIngredient19().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient19();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient19();
            }
        }
        if (recipeDetailsIM.getStrIngredient20() != null && !(recipeDetailsIM.getStrIngredient20().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allIngredients = recipeDetailsIM.getStrIngredient20();
            }
            else {
                allIngredients += ", " + recipeDetailsIM.getStrIngredient20();
            }
        }
        return allIngredients;
    }
    // function that gives back a String with all measurements
    public String getAllMeasurements(RecipeDetailsIM recipeDetailsIM) {
        String allMeasurements = "";
        boolean firstIngredient = true;
        if (recipeDetailsIM.getStrMeasure1() != null && !(recipeDetailsIM.getStrMeasure1().trim().length() == 0)) {
            firstIngredient = false;
            allMeasurements = recipeDetailsIM.getStrMeasure1();
        }
        if (recipeDetailsIM.getStrMeasure2() != null && !(recipeDetailsIM.getStrMeasure2().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure2();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure2();
            }
        }
        if (recipeDetailsIM.getStrMeasure3() != null && !(recipeDetailsIM.getStrMeasure3().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure3();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure3();
            }
        }
        if (recipeDetailsIM.getStrMeasure4() != null && !(recipeDetailsIM.getStrMeasure4().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure4();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure4();
            }
        }
        if (recipeDetailsIM.getStrMeasure5() != null && !(recipeDetailsIM.getStrMeasure5().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure5();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure5();
            }
        }
        if (recipeDetailsIM.getStrMeasure6() != null && !(recipeDetailsIM.getStrMeasure6().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure6();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure6();
            }
        }
        if (recipeDetailsIM.getStrMeasure7() != null && !(recipeDetailsIM.getStrMeasure7().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure7();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure7();
            }
        }
        if (recipeDetailsIM.getStrMeasure8() != null && !(recipeDetailsIM.getStrMeasure8().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure8();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure8();
            }
        }
        if (recipeDetailsIM.getStrMeasure9() != null && !(recipeDetailsIM.getStrMeasure9().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure9();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure9();
            }
        }
        if (recipeDetailsIM.getStrMeasure10() != null && !(recipeDetailsIM.getStrMeasure10().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure10();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure10();
            }
        }
        if (recipeDetailsIM.getStrMeasure11() != null && !(recipeDetailsIM.getStrMeasure11().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure11();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure11();
            }
        }
        if (recipeDetailsIM.getStrMeasure12() != null && !(recipeDetailsIM.getStrMeasure12().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure12();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure12();
            }
        }
        if (recipeDetailsIM.getStrMeasure13() != null && !(recipeDetailsIM.getStrMeasure13().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure13();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure13();
            }
        }
        if (recipeDetailsIM.getStrMeasure14() != null && !(recipeDetailsIM.getStrMeasure14().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure14();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure14();
            }
        }
        if (recipeDetailsIM.getStrMeasure15() != null && !(recipeDetailsIM.getStrMeasure15().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure15();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure15();
            }
        }
        if (recipeDetailsIM.getStrMeasure16() != null && !(recipeDetailsIM.getStrMeasure16().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure16();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure16();
            }
        }
        if (recipeDetailsIM.getStrMeasure17() != null && !(recipeDetailsIM.getStrMeasure17().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements =recipeDetailsIM.getStrMeasure17();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure17();
            }
        }
        if (recipeDetailsIM.getStrMeasure18() != null && !(recipeDetailsIM.getStrMeasure18().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure18();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure18();
            }
        }
        if (recipeDetailsIM.getStrMeasure19() != null && !(recipeDetailsIM.getStrMeasure19().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure19();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure19();
            }
        }
        if (recipeDetailsIM.getStrMeasure20() != null && !(recipeDetailsIM.getStrMeasure20().trim().length() == 0)) {
            if (firstIngredient) {
                firstIngredient = false;
                allMeasurements = recipeDetailsIM.getStrMeasure20();
            }
            else {
                allMeasurements += ", " + recipeDetailsIM.getStrMeasure20();
            }
        }
        return allMeasurements;
    }
}
