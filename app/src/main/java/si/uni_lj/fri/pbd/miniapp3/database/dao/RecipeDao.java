package si.uni_lj.fri.pbd.miniapp3.database.dao;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;

@Dao
public interface RecipeDao {

    // get recipe by idMeal (call this when check if the recipe is in favorites)
    @Query("SELECT * FROM RecipeDetails WHERE idMeal = :idMeal")
    RecipeDetails getRecipeById(String idMeal);

    // insert recipe (call this when the recipe is added to favorites)
    @Insert
    void insertRecipe(RecipeDetails recipeDetails);

    @Query("DELETE FROM RecipeDetails WHERE idMeal = :idMeal")
    void delete(String idMeal);

    // get all favorites (call this when open the Favorites)
    @Query("SELECT * FROM RecipeDetails")
    List<RecipeDetails> getAllFavorites();

}
