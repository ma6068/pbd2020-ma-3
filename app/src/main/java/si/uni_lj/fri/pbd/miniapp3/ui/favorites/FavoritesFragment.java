package si.uni_lj.fri.pbd.miniapp3.ui.favorites;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails;
import si.uni_lj.fri.pbd.miniapp3.models.viewModel.FavoriteViewModel;
import si.uni_lj.fri.pbd.miniapp3.models.Mapper;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;


public class FavoritesFragment extends Fragment {

    private RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerView recyclerView;
    private List<RecipeDetails> recipeDetails;
    private RecyclerView.LayoutManager layoutManager;
    private FavoriteViewModel favoriteViewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_favorites, container, false);

        // get the recycleView
        recyclerView = root.findViewById(R.id.recycler_view2);

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();

        // get the data
        favoriteViewModel = new ViewModelProvider(this).get(FavoriteViewModel.class);
        recipeDetails = favoriteViewModel.getAllFavoriteRecipes();

        // transform data to recipeSummaryIM object
        List<RecipeSummaryIM> recipeSummaryIMS = new ArrayList<RecipeSummaryIM>();
        if (recipeDetails.size() != 0) {
            recyclerView.setVisibility(View.VISIBLE);
            for (int i=0; i<recipeDetails.size(); i++) {
                RecipeSummaryIM recipeSummaryIM = Mapper.mapRecipeDetailsToRecipeSummaryIm(recipeDetails.get(i));
                recipeSummaryIMS.add(recipeSummaryIM);
                // show the recipes via RecyclerViewAdapter
                layoutManager = new LinearLayoutManager(getContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), recipeSummaryIMS, "2");
                recyclerView.setAdapter(recyclerViewAdapter);
            }
        }
        else {
            // No favorite recipes => hide the recyclerView
            recyclerView.setVisibility(View.GONE);
        }
    }
}
