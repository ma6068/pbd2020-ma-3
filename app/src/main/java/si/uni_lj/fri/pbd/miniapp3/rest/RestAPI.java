package si.uni_lj.fri.pbd.miniapp3.rest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIdDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;

public interface RestAPI {

    // list of all the ingredients
    @GET("list.php?i=list")
    Call<IngredientsDTO> getAllIngredients();

    // list of all recipes by ingredient
    @GET("filter.php")
    Call<RecipesByIngredientDTO> getRecipesByIngredient(@Query("i") String ingredient);

    // list of all recipes by id
    @GET("lookup.php")
    Call<RecipesByIdDTO> getRecipesById(@Query("i") String id);

}