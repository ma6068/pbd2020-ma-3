package si.uni_lj.fri.pbd.miniapp3.ui.search;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.uni_lj.fri.pbd.miniapp3.R;
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter;
import si.uni_lj.fri.pbd.miniapp3.adapter.SpinnerAdapter;
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeSummaryDTO;
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO;
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI;
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator;

public class SearchFragment extends Fragment {

    private Spinner spinner;
    private SpinnerAdapter spinnerAdapter;
    private TextView errorTextView;
    private TextView textView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<IngredientDTO> ingredients;
    private MaterialProgressBar materialProgressBar;

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search, container, false);

        // set the spinner
        spinner = root.findViewById(R.id.spinner);

        // set the textView
        textView = root.findViewById(R.id.textView);

        // set the errorTextView
        errorTextView = root.findViewById(R.id.textView_error);

        // set the recycleView
        recyclerView = root.findViewById(R.id.recycler_view);

        // set the swipeRefreshLayout
        swipeRefreshLayout = root.findViewById(R.id.swipeRefreshLayout);

        // set the MaterialProgressBar
        materialProgressBar = root.findViewById(R.id.materialProgressBar);

        // get the ingredients
        apiGetIngredients();

        // set onRefresh
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // when tha app starts there is no connection => get the ingredients
                if (ingredients == null) {
                    apiGetIngredients();
                }
                // no connection while using the app => refresh the recipes
                else {
                    getRecipesOnSelectedIngredient();
                }
                new Handler().postDelayed(() -> swipeRefreshLayout.setRefreshing(false), 5000);
            }
        });

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        getRecipesOnSelectedIngredient();
    }

    public void getRecipesOnSelectedIngredient() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // show the MaterialProgressBar
                materialProgressBar.setVisibility(View.VISIBLE);
                // download the recipe with chosen ingredient via API
                RestAPI restAPI =  ServiceGenerator.createService(RestAPI.class);
                Call<RecipesByIngredientDTO> call = restAPI.getRecipesByIngredient(spinnerAdapter.getItem(position).getStrIngredient());
                call.enqueue(new Callback<RecipesByIngredientDTO>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(Call<RecipesByIngredientDTO> call, Response<RecipesByIngredientDTO> response) {
                        // response is not successful
                        if (!response.isSuccessful()) {
                            materialProgressBar.setVisibility(View.GONE);
                            Snackbar.make(errorTextView, "Code: " + response.code(), Snackbar.LENGTH_LONG).show();
                        }
                        // the response is successful
                        else {
                            // get the response
                            RecipesByIngredientDTO recipesByIngredientDTO = response.body();
                            // get the list of all recipes
                            List<RecipeSummaryDTO> recipes = recipesByIngredientDTO.getRecipes();
                            if (recipes == null) {
                                materialProgressBar.setVisibility(View.GONE);
                                Snackbar.make(errorTextView, "Sorry, no recipes for this ingredient exist", Snackbar.LENGTH_LONG).show();
                            }
                            else {
                                // clear the errorTextView and materialProgressBar
                                errorTextView.setText("");
                                materialProgressBar.setVisibility(View.GONE);
                                // set the textView with chosen ingredient name
                                textView.setText(spinnerAdapter.getItem(position).getStrIngredient());
                                // show the recipes via RecyclerViewAdapter
                                layoutManager = new LinearLayoutManager(view.getContext());
                                recyclerView.setLayoutManager(layoutManager);
                                // transform to RecipeSummaryIM objects
                                List<RecipeSummaryIM> recipeSummaryIMS = new ArrayList<RecipeSummaryIM>();
                                for (int i=0; i<recipes.size(); i++) {
                                    RecipeSummaryDTO recipeSummaryDTO = recipes.get(i);
                                    RecipeSummaryIM recipeSummaryIM = new RecipeSummaryIM(recipeSummaryDTO.getSrtMeal(), recipeSummaryDTO.getStrMealThumb(), recipeSummaryDTO.getIdMeal());
                                    recipeSummaryIMS.add(recipeSummaryIM);
                                }
                                // call the adapter
                                recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), recipeSummaryIMS, "1");
                                recyclerView.setAdapter(recyclerViewAdapter);
                            }
                        }
                    }
                    // failed to response
                    @Override
                    public void onFailure(Call<RecipesByIngredientDTO> call, Throwable t) {
                        Snackbar.make(errorTextView, "No internet connection", Snackbar.LENGTH_LONG).show();
                        materialProgressBar.setVisibility(View.GONE);
                    }
                });
            }
            // close the onItemSelected
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    public void apiGetIngredients() {
        // call the api to get all ingredients
        RestAPI restAPI =  ServiceGenerator.createService(RestAPI.class);
        Call<IngredientsDTO> call = restAPI.getAllIngredients();
        call.enqueue(new Callback<IngredientsDTO>() {
            // we have a response
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<IngredientsDTO> call, Response<IngredientsDTO> response) {
                // response is not successful
                if (!response.isSuccessful()) {
                    Snackbar.make(errorTextView, "Code: " + response.code(), Snackbar.LENGTH_LONG).show();
                }
                // the response is successful
                else {
                    // get the response (IngredientsDTO object)
                    IngredientsDTO ingredientsDTO = response.body();
                    ingredients = ingredientsDTO.getIngredients();
                    if (ingredients == null) {
                        // show message that there are no ingredients
                        Snackbar.make(errorTextView, "There are no ingredients", Snackbar.LENGTH_LONG).show();
                    }
                    else {
                        // show ingredients via SpinnerAdapter
                        spinnerAdapter = new SpinnerAdapter(getActivity(), ingredients);
                        spinner.setAdapter(spinnerAdapter);
                    }
                }
            }
            // failed to response
            @Override
            public void onFailure(Call<IngredientsDTO> call, Throwable t) {
                Snackbar.make(errorTextView, "No internet connection", Snackbar.LENGTH_LONG).show();
            }
        });
    }
}
